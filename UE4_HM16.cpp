﻿#include <iostream>
#include <time.h>

int main()
{
	const int n = 5;
	int array[n][n];
	int sum = 0;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int i = buf.tm_wday % n;

	for (int j = 0; j < n; j++)
	{
		sum += array[i][j];
	}
	std::cout << "The sum of " << i +1 << " row is " << sum << std::endl;
}